package md.utm.labs.pr.mime;

import java.io.InputStream;

import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MimeTypes;

public class MimeUtil {
	private static final Detector DETECTOR = new DefaultDetector(MimeTypes.getDefaultMimeTypes());
	
	public String detectMimeType(InputStream inputStream) {
		TikaInputStream stream = null;
	    try {
	    	stream = TikaInputStream.get(inputStream);
	        final Metadata metadata = new Metadata();
	        return DETECTOR.detect(stream, metadata).toString();
	    } catch (Exception e) {
	    	throw new RuntimeException();
	    } finally {
	        if (stream != null) {
	        	try {
	        		stream.close();
	        	} catch (Exception e) {
	        		throw new RuntimeException(e);
	        	}
	        }
	    }
	}
}
