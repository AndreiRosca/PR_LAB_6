package md.utm.labs.pr.decoder;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import md.utm.labs.pr.mime.MimeUtil;

public class TcpStreamMimeDecoder {
	private static final int TCP_HEADER_SIZE = 66;
	private static final Map<String, String> mimeTypes = new HashMap<>();

	private MimeUtil util = new MimeUtil();

	static {
		mimeTypes.put("image/png", ".png");
		mimeTypes.put("image/gif", ".gif");
		mimeTypes.put("image/jpeg", ".jpg");
		mimeTypes.put("application/octet-stream", ".bin");
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Wrong number of arguments.");
			return;
		}
		TcpStreamMimeDecoder decoder = new TcpStreamMimeDecoder();
		String mimeType = decoder.decode(args[0]);
		System.out.println(decoder.getFileExtesionFor(mimeType));
	}

	private String getFileExtesionFor(String mimeType) {
		return mimeTypes.getOrDefault(mimeType, ".bin");
	}

	public String decode(String filePath) {
		byte[] fileBody = getFileBody(filePath);
		byte[] tcpSegmentData = getTcpSegmentData(fileBody);
		return util.detectMimeType(new ByteArrayInputStream(tcpSegmentData));
	}

	private byte[] getTcpSegmentData(byte[] tcpSegment) {
		int httpBodyIndex = getBodyIndex(tcpSegment, TCP_HEADER_SIZE);
		return Arrays.copyOfRange(tcpSegment, httpBodyIndex, tcpSegment.length);
	}

	private int getBodyIndex(byte[] httpResponse, int startingIndex) {
		for (int i = startingIndex; i < httpResponse.length; ++i)
			if (isNewLine(httpResponse, i) && isNewLine(httpResponse, i + 2))
				return i + 4;
		return -1;
	}

	private boolean isNewLine(byte[] httpResponse, int i) {
		return httpResponse[i] == 0xD && httpResponse[i + 1] == 0xA;
	}

	private byte[] getFileBody(String file) {
		try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
			ByteArrayOutputStream destination = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = in.read(buffer)) != -1)
				destination.write(buffer, 0, bytesRead);
			return destination.toByteArray();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
